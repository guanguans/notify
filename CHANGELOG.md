# Changelog

All notable changes to `guanguans/notify` will be documented in this file.

## 1.0.0 - 2021-05-16

* Initial release.

## 1.0.1 - 2021-05-16

Add separate method for separate setting option.
Perf messages.
Perf clients.
